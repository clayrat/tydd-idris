module Main 

import Data.Fin
import Data.Vect

data BSTree : Type -> Type where
  Empty : Ord elem => BSTree elem
  Node : Ord elem => (left : BSTree elem) 
                  -> (val : elem) 
                  -> (right : BSTree elem) 
                  -> BSTree elem

insert : elem -> BSTree elem -> BSTree elem
insert x Empty = Node Empty x Empty
insert x orig@(Node left val right) = case 
  compare x val of
    LT => Node (insert x left) val right
    EQ => orig
    GT => Node left val (insert x right)

listToTree : Ord a => List a -> BSTree a
listToTree [] = Empty
listToTree (x :: xs) = insert x (listToTree xs)

treeToList : BSTree a -> List a
treeToList Empty = []
treeToList (Node left val right) = treeToList left ++ val :: treeToList right

data Expr = Val Int | Add Expr Expr | Sub Expr Expr | Mul Expr Expr

evaluate : Expr -> Int
evaluate (Val val) = val
evaluate (Add l r) = evaluate l + evaluate r
evaluate (Sub l r) = evaluate l - evaluate r
evaluate (Mul l r) = evaluate l * evaluate r

maxMaybe : Ord a => Maybe a -> Maybe a -> Maybe a
maxMaybe Nothing Nothing = Nothing
maxMaybe (Just x) Nothing = Just x
maxMaybe Nothing (Just y) = Just y
maxMaybe (Just x) (Just y) = Just (max x y)

data Shape = Triangle Double Double
 | Rectangle Double Double
 | Circle Double

area : Shape -> Double
area (Triangle base height) = 0.5 * base * height
area (Rectangle length height) = length * height
area (Circle radius) = pi * radius * radius

data Picture = Primitive Shape
 | Combine Picture Picture
 | Rotate Double Picture
 | Translate Double Double Picture

biggestTriangle : Picture -> Maybe Double
biggestTriangle (Primitive x) = case x of
    s@(Triangle _ _) => Just (area s)
    Rectangle _ _ => Nothing
    Circle _ => Nothing
biggestTriangle (Combine x y) = maxMaybe (biggestTriangle x) (biggestTriangle y)
biggestTriangle (Rotate _ p) = biggestTriangle p
biggestTriangle (Translate _ _ p) = biggestTriangle p

testPic1 : Picture
testPic1 = Combine (Primitive (Triangle 2 3))
  (Primitive (Triangle 2 4))

testPic2 : Picture
testPic2 = Combine (Primitive (Rectangle 1 3))
  (Primitive (Circle 4))

data PowerSource = Petrol | Pedal | Electricity
data Vehicle : PowerSource -> Type where
  Bicycle : Vehicle Pedal
  Unicycle : Vehicle Pedal
  Motorcycle : (fuel : Nat) -> Vehicle Petrol
  Car : (fuel : Nat) -> Vehicle Petrol
  ElectricCar : (fuel : Nat) -> Vehicle Electricity
  Bus : (fuel : Nat) -> Vehicle Petrol

wheels : Vehicle power -> Nat
wheels Bicycle = 2
wheels Unicycle  = 1
wheels (Motorcycle fuel) = 2
wheels (Car fuel) = 4
wheels (ElectricCar fuel) = 4
wheels (Bus fuel) = 4

refuel : Vehicle Petrol -> Vehicle Petrol
refuel (Motorcycle fuel) = (Motorcycle 50)
refuel (Car fuel) = Car 100
refuel (Bus fuel) = Bus 200

vectTake : (x : Fin n) -> Vect n a -> Vect (finToNat x) a
vectTake FZ _ = []
vectTake (FS _) [] impossible
vectTake (FS x) (y :: xs) = y :: vectTake x xs

sumEntries : Num a => (pos : Integer) -> Vect n a -> Vect n a -> Maybe a
sumEntries {n} pos xs ys = (\f => index f xs + index f ys) <$> (integerToFin pos n)
