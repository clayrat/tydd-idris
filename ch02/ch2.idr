module Main

abc : (String, String, String)
abc = ("A", "B", "C")

abc2 : List String
abc2 = ["A", "B", "C"]

abc3 : ((Char, String), Char)
abc3 = (('A', "B"), 'C')

palindrome : Nat -> String -> Bool
palindrome n s = 
 if length s > n then
  let
   l = toLower s
  in
   reverse l == l
 else False

counts : String -> (Nat, Nat)
counts s = let
    w = length $ words s
    c = length s
  in
    (w,c)

top_ten : Ord a => List a -> List a 
top_ten = List.take 10 . reverse . sort

over_length : Nat -> List String -> Nat
over_length n = length . filter (> n) . map length

replPalindrome : IO ()
replPalindrome = repl "Enter a string: " (\s => (show $ palindrome 10 s) ++ "\n")

replCounts : IO ()
replCounts = repl "Enter a string: " (\s => (show $ counts s) ++ "\n")

main : IO ()
main = replPalindrome