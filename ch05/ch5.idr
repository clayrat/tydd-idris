module Main

import System 
import Data.Vect

printLonger : IO ()
printLonger = do
  putStr "1)"
  a <- getLine
  putStr "2)"
  b <- getLine
  putStrLn $ show $ max (length a) (length b)

printLongerK : IO ()
printLongerK = 
  putStr "1)" 
    >>= \_ =>
    getLine 
      >>= \a =>
      putStr "2)" 
        >>= \_ =>
        getLine 
          >>= \b =>
          putStrLn $ show $ max (length a) (length b)

readNumber : IO (Maybe Nat)
readNumber = do
  input <- getLine
  if all isDigit (unpack input)
    then pure (Just (cast input))
    else pure Nothing

guess : (target : Nat) -> (guesses : Nat) -> IO ()
guess target guesses = do
  putStr ("Make a guess (" ++ show guesses ++ "): ")
  Just num <- readNumber 
    | Nothing => do putStrLn "Not a number"
                    guess target guesses
  case compare num target of
    LT => do putStrLn "Higher"
             guess target (guesses+1)
    GT => do putStrLn "Lower"
             guess target (guesses+1)
    EQ => do putStrLn "Correct!"
             pure ()

rnd : IO Integer
rnd = (\x => x `mod` 100) <$> System.time 

repl2 : (prompt : String) -> (onInput : String -> String) -> IO ()
repl2 prompt onInput = do
    putStr prompt
    i <- getLine
    putStr (onInput i)
    repl2 prompt onInput

replWith2 : (state : a) -> (prompt : String) -> (onInput : a -> String -> Maybe (String, a)) -> IO ()     
replWith2 state prompt onInput = do
    putStr prompt
    i <- getLine
    case onInput state i of 
      Just (o, a1) => do putStr o
                         replWith a1 prompt onInput
      Nothing => pure ()

readToBlank : IO (List String)
readToBlank = do
    i <- getLine
    if length i == 0
      then pure [] 
      else (i ::) <$> readToBlank 

readAndSave : IO ()
readAndSave = do
    xs <- readToBlank
    fname <- getLine
    putStr "File to save: "
    Right () <- writeFile fname (unwords xs) 
     | Left err => do printLn err
                      pure ()
    pure ()    

readVect : IO (len ** Vect len String)
readVect = do x <- getLine
              if x == ""
                then pure (_ ** [])
                else do (_ ** xs) <- readVect
                        pure (_ ** x :: xs)
            
readVectFile : (filename : String) -> IO (n ** Vect n String)    
readVectFile filename = do
    Right contents <- readFile filename
      | Left err => do printLn err
                       pure (_ ** [])
    pure (_ ** fromList (lines contents))

main : IO ()
main = do
     n <- rnd
     guess (toNat n) 0