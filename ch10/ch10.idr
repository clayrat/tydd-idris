module Main

import Data.List.Views

import Data.Vect
import Data.Vect.Views

import Data.Nat.Views

import DataStore

import Shape_abs

data TakeN : List a -> Type where
  Fewer : TakeN xs
  Exact : (n_xs : List a) -> TakeN (n_xs ++ rest)

takeN : (n : Nat) -> (xs : List a) -> TakeN xs
takeN Z xs = Exact []
takeN (S _) [] = Fewer 
takeN (S k) (x :: xs) with (takeN k xs)
  takeN (S k) (x :: xs) | Fewer = Fewer
  takeN (S k) (x :: (n_xs ++ rest)) | Exact _ = Exact (x :: n_xs)
      
groupByN : (n : Nat) -> (xs : List a) -> List (List a)
groupByN n xs with (takeN n xs)
  groupByN n xs | Fewer = [xs]
  groupByN n (n_xs ++ rest) | (Exact n_xs) = n_xs :: groupByN n rest

halves : List a -> (List a, List a)
halves xs with (takeN (length xs `div` 2) xs)
  halves xs | Fewer = (xs, [])
  halves (n_xs ++ rest) | (Exact n_xs) = (n_xs, rest)

equalSuffix : Eq a => List a -> List a -> List a
equalSuffix xs ys with (snocList xs)
  equalSuffix [] ys | Empty = []
  equalSuffix (xs' ++ [x]) ys | (Snoc xrec) with (snocList ys)
    equalSuffix (xs' ++ [x]) [] | (Snoc xrec) | Empty = []
    equalSuffix (xs' ++ [x]) (ys' ++ [y]) | (Snoc xrec) | (Snoc yrec) = 
      if x == y 
        then (equalSuffix xs' ys' | xrec | yrec) ++ [x] 
        else []

mergeSort : Ord a => Vect n a -> Vect n a
mergeSort input with (splitRec input)
  mergeSort [] | SplitRecNil = []
  mergeSort [x] | SplitRecOne = [x]
  mergeSort (lefts ++ rights) | (SplitRecPair lrec rrec)
    = merge (mergeSort lefts | lrec) (mergeSort rights | rrec)

toBinary : Nat -> String
toBinary Z = "0"
toBinary k with (halfRec k)
  toBinary Z | HalfRecZ = ""
  toBinary (n + n) | (HalfRecEven rec) = toBinary n | rec ++ "0"
  toBinary (S (n + n)) | (HalfRecOdd rec) = toBinary n | rec ++ "1"

palindrome : Eq a => List a -> Bool
palindrome xs with (vList xs)
  palindrome _ | VNil = True
  palindrome _ | VOne = True
  palindrome (h :: (xs' ++ [t])) | (VCons rec) = 
    if h == t 
      then palindrome xs' | rec 
      else False

getValues : DataStore (SString .+. val_schema) -> List (SchemaType val_schema)
getValues x with (storeView x)
  getValues _ | SNil = []
  getValues (addToStore (_,v) store) | (SAdd rec) = v :: getValues store | rec

testStore : DataStore (SString .+. SInt)
testStore = addToStore ("First", 1) $
            addToStore ("Second", 2) $
            empty

area : Shape -> Double
area s with (shapeView s)
  area (triangle base height) | STriangle = 0.5 * base * height
  area (rectangle width height) | SRectangle = width * height
  area (circle radius) | SCircle = pi * radius * radius
