module Main

import Data.Primitives.Views

%default total

every_other : Stream a -> Stream a
every_other (_ :: value :: xs) = value :: every_other xs

data InfList : Type -> Type where
  (::) : (value : elem) -> Inf (InfList elem) -> InfList elem

%name InfList xs, ys, zs

countFrom : Integer -> InfList Integer
countFrom x = x :: countFrom (x + 1)

getPrefix : Nat -> InfList a -> List a
getPrefix Z x = []
getPrefix (S k) (x :: xs) = x :: getPrefix k xs

Functor InfList where
  map f (x :: xs) = f x :: map f xs

randoms : Int -> Stream Int
randoms seed = let 
    seed' = 1664525 * seed + 1013904223 
  in
    (seed' `shiftR` 2) :: randoms seed'

data Face = Heads | Tails

coinFlips : (count : Nat) -> Stream Int -> List Face  
coinFlips c s = take c $ getFace <$> s
  where 
    getFace : Int -> Face
    getFace x with (divides x 2)
      getFace ((2 * _) + rem) | (DivBy _) = if rem == 1 then Tails else Heads

square_root_approx : (number : Double) -> (approx : Double) -> Stream Double
square_root_approx number approx = approx :: square_root_approx number next
  where 
    next = (approx + (number / approx)) / 2

square_root_bound : (max : Nat) -> (number : Double) -> (bound : Double) -> (approxs : Stream Double) -> Double
square_root_bound Z _ _ (x :: _) = x
square_root_bound (S k) number bound (x :: xs) = 
  if abs (number - x*x) < bound 
    then x 
    else square_root_bound k number bound xs

square_root : (number : Double) -> Double
square_root number = square_root_bound 100 number 0.00000000001 (square_root_approx number number)

data InfIO : Type where
  Do : IO a -> (a -> Inf InfIO) -> InfIO
 
(>>=) : IO a -> (a -> Inf InfIO) -> InfIO
(>>=) = Do

data Fuel = Dry | More (Lazy Fuel)

partial
forever : Fuel
forever = More forever

run : Fuel -> InfIO -> IO ()
run (More fuel) (Do c f) = do 
  res <- c
  run fuel (f res)
run Dry p = putStrLn "Out of fuel"

totalREPL : (prompt : String) -> (action : String -> String) -> InfIO
totalREPL prompt action = do 
  putStr prompt                                                                         
  x <- getLine              
  putStr $ action x                                                
  totalREPL prompt action                  
