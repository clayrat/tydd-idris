import Data.Primitives.Views
import System

%default total

data Command : Type -> Type where
     PutStr : String -> Command ()
     GetLine : Command String

     Pure : ty -> Command ty
     Bind : Command a -> (a -> Command b) -> Command b

data ConsoleIO : Type -> Type where
     Quit : a -> ConsoleIO a
     Do : Command a -> (a -> Inf (ConsoleIO b)) -> ConsoleIO b

namespace CommandDo
  (>>=) : Command a -> (a -> Command b) -> Command b
  (>>=) = Bind

namespace ConsoleDo
  (>>=) : Command a -> (a -> Inf (ConsoleIO b)) -> ConsoleIO b
  (>>=) = Do

data Fuel = Dry | More (Lazy Fuel)

runCommand : Command a -> IO a
runCommand (PutStr x) = putStr x
runCommand GetLine = getLine
runCommand (Pure val) = pure val
runCommand (Bind c f) = do res <- runCommand c
                           runCommand (f res)

run : Fuel -> ConsoleIO a -> IO (Maybe a)
run fuel (Quit val) = do pure (Just val)
run (More fuel) (Do c f) = do res <- runCommand c
                              run fuel (f res)
run Dry p = pure Nothing

randoms : Int -> Stream Int
randoms seed = let seed' = 1664525 * seed + 1013904223 in
                   (seed' `shiftR` 2) :: randoms seed'

arithInputs : Int -> Stream Int
arithInputs seed = map bound (randoms seed)
  where
    bound : Int -> Int
    bound x with (divides x 12)
      bound ((12 * div) + rem) | (DivBy prf) = abs rem + 1

mutual
  correct : Stream Int -> (score : Nat) -> (tot : Nat) -> ConsoleIO (Nat, Nat)
  correct nums score tot
          = do PutStr "Correct!\n"
               quizHelper nums (score + 1) (tot + 1)

  wrong : Stream Int -> Int -> (score : Nat) -> (tot : Nat) -> ConsoleIO (Nat, Nat)
  wrong nums ans score tot
        = do PutStr ("Wrong, the answer is " ++ show ans ++ "\n")
             quizHelper nums score (tot + 1)

  data Input = Answer Int
             | QuitCmd

  readInput : (prompt : String) -> Command Input
  readInput prompt 
     = do PutStr prompt
          answer <- GetLine
          if toLower answer == "quit" 
             then Pure QuitCmd 
             else Pure (Answer (cast answer))

  quizHelper : Stream Int -> (score : Nat) -> (tot : Nat) -> ConsoleIO (Nat, Nat)
  quizHelper (num1 :: num2 :: nums) score tot
     = do PutStr ("Score so far: " ++ show score ++ " / " ++ show tot ++ "\n")
          input <- readInput (show num1 ++ " * " ++ show num2 ++ "? ")
          case input of
               Answer answer => if answer == num1 * num2 
                                   then correct nums score tot
                                   else wrong nums (num1 * num2) score tot
               QuitCmd => Quit (score, tot)

quiz : Stream Int -> ConsoleIO (Nat, Nat)
quiz nums = quizHelper nums 0 0

partial
forever : Fuel
forever = More forever

partial
main : IO ()
main = do seed <- time
          Just (score, tot) <- run forever $ quiz $ arithInputs $ fromInteger seed
               | Nothing => putStrLn "Ran out of fuel"
          putStrLn ("Final score: " ++ show score ++ " / " ++ show tot)
