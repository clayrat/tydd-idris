%default total

data Command : Type -> Type where
  PutStr : String -> Command ()
  GetLine : Command String
  ReadFile : String -> Command (Either FileError String)  
  WriteFile : (filepath : String) -> (contents : String) -> Command (Either FileError ())     
  Pure : ty -> Command ty
  Bind : Command a -> (a -> Command b) -> Command b                     

data ConsoleIO : Type -> Type where
  Quit : a -> ConsoleIO a
  Do : Command a -> (a -> Inf (ConsoleIO b)) -> ConsoleIO b

namespace CommandDo
  (>>=) : Command a -> (a -> Command b) -> Command b
  (>>=) = Bind

namespace ConsoleDo
  (>>=) : Command a -> (a -> Inf (ConsoleIO b)) -> ConsoleIO b
  (>>=) = Do

data Fuel = Dry | More (Lazy Fuel)

runCommand : Command a -> IO a
runCommand (PutStr x) = putStr x
runCommand GetLine = getLine
runCommand (ReadFile path) = readFile path
runCommand (WriteFile path c) = writeFile path c  -- fixed for Win in https://github.com/idris-lang/Idris-dev/pull/3820
runCommand (Pure val) = pure val
runCommand (Bind c f) = do 
  res <- runCommand c
  runCommand (f res)

run : Fuel -> ConsoleIO a -> IO (Maybe a)
run fuel (Quit val) = do pure (Just val)
run (More fuel) (Do c f) = do res <- runCommand c
                              run fuel (f res)
run Dry p = pure Nothing

data Input = CatCmd String | CopyCmd String String | ExitCmd | Wrong

readInput : Command Input
readInput = do 
  answer <- GetLine
  let wrd = words $ toLower answer
  Pure $ case wrd of
    ["cat", fname] => CatCmd fname
    ["copy", from, to] => CopyCmd from to
    ["exit"] => ExitCmd 
    _ => Wrong

mutual 
  invalid : ConsoleIO ()
  invalid = do
    PutStr "Invalid command!\n"
    shell
 
  err : String -> ConsoleIO ()
  err msg = do
    PutStr $ msg ++ "\n"
    shell

  cat : String -> ConsoleIO ()
  cat fname = do
     Right contents <- ReadFile fname
       | Left _ => err ("Error reading file " ++ fname)
     PutStr contents 
     shell

  copy : String -> String -> ConsoleIO ()
  copy from to = do
     Right contents <- ReadFile from
       | Left _ => err ("Error reading file " ++ from)
     Right _ <- WriteFile to contents   
       | Left _ => err ("Error writing file " ++ to)
     shell

  shell : ConsoleIO ()
  shell = 
    do PutStr "Enter command: "
       input <- readInput 
       case input of
         Wrong => invalid
         (CatCmd fname) => cat fname
         (CopyCmd from to) => copy from to
         QuitCmd => Quit ()

partial
forever : Fuel
forever = More forever

partial
main : IO ()
main = do
  run forever shell
  pure ()