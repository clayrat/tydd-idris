module Main 

import Data.Vect

length2 : List a -> Nat
length2 [] = 0
length2 (x :: xs) = 1 + length2 xs

reverse2 : List a -> List a
reverse2 [] = []
reverse2 (x :: xs) = reverse xs ++ [x]

map2 : (a -> b) -> List a -> List b
map2 f [] = []
map2 f (x :: xs) = f x :: map2 f xs

map2V : (a -> b) -> Vect n a -> Vect n b
map2V f [] = []
map2V f (x :: xs) = f x :: map2V f xs

createEmpties : Vect n (Vect 0 elem)
createEmpties = replicate _ []

transposeMat : Vect m (Vect n elem) -> Vect n (Vect m elem)
transposeMat [] = createEmpties
transposeMat (x :: xs) = let 
    xsTrans = transposeMat xs 
  in
    zipWith (\a,b => (a :: b)) x xsTrans

addMatrix : Num a => Vect n (Vect m a) -> Vect n (Vect m a) -> Vect n (Vect m a)
addMatrix [] [] = []
addMatrix (x :: xs) (y :: ys) = zipWith (\a,b => (a + b)) x y :: addMatrix xs ys

product : Num a => Vect m a -> Vect m a -> a
product a b = sum $ zipWith (*) a b

helper : Num a => Vect n (Vect m a) -> Vect p (Vect m a) -> Vect n (Vect p a)
helper {n} _ [] = createEmpties {n}
helper {p} [] _ = []
helper (x :: xs) ys = map (product x) ys :: helper xs ys

multMatrix : Num a => Vect n (Vect m a) -> Vect m (Vect p a) -> Vect n (Vect p a)
multMatrix xs ys = helper xs (transposeMat ys)

main : IO ()
main = pure ()