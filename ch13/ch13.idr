namespace Guess 
  data GuessCmd : Type -> Nat -> Nat -> Type where
     Try : Integer -> GuessCmd Ordering (S k) k
     Pure : ty -> GuessCmd ty state state
     (>>=) : GuessCmd a state1 state2 -> (a -> GuessCmd b state2 state3) -> GuessCmd b state1 state3
  
  threeGuesses: GuessCmd () 3 0
  threeGuesses = do
    Try 10
    Try 20
    Try 15
    Pure ()

namespace Matter
  data Matter = Solid | Liquid | Gas
  
  data MatterCmd : Type -> Matter -> Matter -> Type where
    Melt : MatterCmd () Solid Liquid
    Boil : MatterCmd () Liquid Gas 
    Condense : MatterCmd () Gas Liquid
    Freeze : MatterCmd () Liquid Solid
    Pure : ty -> MatterCmd ty s s
    (>>=) : MatterCmd a s1 s2 -> (a -> MatterCmd b s2 s3) -> MatterCmd b s1 s3
  
  iceSteam : MatterCmd () Solid Gas
  iceSteam = do 
    Melt
    Boil
  
  steamIce : MatterCmd () Gas Solid
  steamIce = do
    Condense
    Freeze