import Control.Monad.State

update : (stateType -> stateType) -> State stateType ()
update f = do
    x <- get
    put $ f x

increase : Nat -> State Nat ()
increase x = update (+x)

data Tree a = Empty
   | Node (Tree a) a (Tree a)

countEmpty : Tree a -> State Nat ()
countEmpty Empty = update (+1)
countEmpty (Node l _ r) = countEmpty l *> countEmpty r

countEmptyNode : Tree a -> State (Nat, Nat) ()
countEmptyNode Empty = update (\(e, n) => (S e, n))
countEmptyNode (Node l _ r) = 
     countEmptyNode l 
  *> update (\(e, n) => (e, S n)) 
  *> countEmptyNode r

testTree : Tree String
testTree = Node 
             (Node 
                (Node Empty "Jim" Empty) 
                "Fred" 
                (Node Empty "Sheila" Empty)
             ) 
             "Alice"
             (Node 
                Empty 
                "Bob" 
                (Node Empty "Eve" Empty)
             )

record Votes where
  constructor MkVotes
  upvotes : Integer
  downvotes : Integer

record Article where
  constructor MkArticle
  title : String
  url : String
  score : Votes

initPage : (title : String) -> (url : String) -> Article
initPage title url = MkArticle title url (MkVotes 0 0)

getScore : Article -> Integer
getScore x = upvotes sc - downvotes sc
  where sc = score x

addUpvote : Article -> Article
addUpvote = record { score->upvotes $= (+1) }

addDownvote : Article -> Article  
addDownvote = record { score->downvotes $= (+1) }

badSite : Article
badSite = MkArticle "Bad Page" "http://example.com/bad" (MkVotes 5 47)

goodSite : Article
goodSite = MkArticle "Good Page" "http://example.com/good" (MkVotes 101 7)