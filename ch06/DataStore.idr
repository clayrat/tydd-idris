module Main

import Data.Vect

infixr 5 .+.

data Schema = SString | SChar | SInt | (.+.) Schema Schema

SchemaType : Schema -> Type
SchemaType SString = String
SchemaType SChar = Char
SchemaType SInt = Int
SchemaType (x .+. y) = (SchemaType x, SchemaType y)

record DataStore where
  constructor MkData
  schema : Schema
  size : Nat
  items : Vect size (SchemaType schema)

addToStore : (store : DataStore) -> SchemaType (schema store) -> DataStore
addToStore (MkData schema size store) newitem = MkData schema _ (addToData store)
  where
    addToData : Vect oldsize (SchemaType schema) -> Vect (S oldsize) (SchemaType schema)
    addToData [] = [newitem]
    addToData (x :: xs) = x :: addToData xs

setSchema : (store : DataStore) -> Schema -> Maybe DataStore
setSchema store schema = 
  case size store of
    Z => Just (MkData schema _ [])
    S k => Nothing

data Command : Schema -> Type where
  SetSchema : Schema -> Command schema
  Add : SchemaType schema -> Command schema
  Get : Integer -> Command schema
  GetAll : Command schema
  Quit : Command schema

parsePrefix : (schema : Schema) -> String -> Maybe (SchemaType schema, String)
parsePrefix SString input = getQuoted (unpack input)
  where
    getQuoted : List Char -> Maybe (String, String)
    getQuoted ('"' :: xs)
        = case span (/= '"') xs of
               (quoted, '"' :: rest) => Just (pack quoted, ltrim (pack rest))
               _ => Nothing
    getQuoted _ = Nothing

parsePrefix SChar input = 
  case span (/= ' ') input of
    (c, rest) => 
      if length c == 1 
        then Just (strHead c, ltrim rest) 
        else Nothing
    _ => Nothing

parsePrefix SInt input = 
  case span isDigit input of
    ("", rest) => Nothing
    (num, rest) => Just (cast num, ltrim rest)

parsePrefix (schemal .+. schemar) input = 
  do
    (l_val, input')  <- parsePrefix schemal input 
    (r_val, input'') <- parsePrefix schemar input' 
    Just ((l_val, r_val), input'')

parseBySchema : (schema : Schema) -> String -> Maybe (SchemaType schema)
parseBySchema schema x = parsePrefix schema x >>= \(res, rest) => 
  if rest == "" 
    then Just res 
    else Nothing

parseSchema : List String -> Maybe Schema
parseSchema ("String" :: xs)
    = case xs of
           [] => Just SString
           _ => (SString .+.) <$> parseSchema xs
parseSchema ("Char" :: xs) 
    = case xs of
           [] => Just SChar
           _ => (SChar .+.) <$> parseSchema xs
parseSchema ("Int" :: xs)
    = case xs of
           [] => Just SInt
           _ => (SInt .+.) <$> parseSchema xs
parseSchema _ = Nothing

parseCommand : (schema : Schema) -> String -> String -> Maybe (Command schema)
parseCommand schema "add" rest = Add <$> parseBySchema schema rest
parseCommand schema "get" val = 
  if val == "" 
    then Just GetAll 
    else
      case all isDigit (unpack val) of
        False => Nothing
        True => Just (Get (cast val))
parseCommand schema "schema" rest = SetSchema <$> parseSchema (words rest) 
parseCommand schema "quit" "" = Just Quit
parseCommand _ _ _ = Nothing

parse : (schema : Schema) -> (input : String) -> Maybe (Command schema)
parse schema input = 
  case span (/= ' ') input of
    (cmd, args) => parseCommand schema cmd (ltrim args)

display : SchemaType schema -> String
display {schema = SString} item = show item
display {schema = SChar} item = show item
display {schema = SInt} item = show item
display {schema = (y .+. z)} (iteml, itemr) = 
  display iteml ++ ", " ++ display itemr

getEntry : (pos : Integer) -> (store : DataStore) ->
           Maybe (String, DataStore)
getEntry pos store = let
    store_items = items store 
  in
    case integerToFin pos (size store) of
      Nothing => Just ("Out of range\n", store)
      Just id => Just (display (index id (items store)) ++ "\n", store)

getAllEntries : (store : DataStore) -> String
getAllEntries store = foldl 
   (\acc, (s,i) => acc ++ show (finToNat i) ++ ": " ++ display s ++ "\n") 
    "" 
  $ Vect.zip (items store) range

processInput : DataStore -> String -> Maybe (String, DataStore)
processInput store input = 
  case parse (schema store) input of
    Nothing => Just ("Invalid command\n", store)
    Just (Add item) =>
       Just ("ID " ++ show (size store) ++ "\n", addToStore store item)
    Just (SetSchema schema') =>
       case setSchema store schema' of
            Nothing => Just ("Can't update schema when entries in store\n", store)
            Just store' => Just ("OK\n", store')
    Just (Get pos) => getEntry pos store
    Just GetAll => Just (getAllEntries store, store)
    Just Quit => Nothing

main : IO ()
main = replWith (MkData (SString .+. SString .+. SInt) _ []) "Command: " processInput
