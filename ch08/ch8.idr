module Main

same_cons : {xs : List a} -> {ys : List a} -> xs = ys -> x :: xs = x :: ys
same_cons Refl = Refl

same_lists : {xs : List a} -> {ys : List a} -> x = y -> xs = ys -> x :: xs = y :: ys
same_lists Refl Refl = Refl

data ThreeEq : a -> b -> c -> Type where
  ThreeSame : ThreeEq a a a

allSameS : (x, y, z : Nat) -> ThreeEq x y z -> ThreeEq (S x) (S y) (S z)  
allSameS _ _ _ ThreeSame = ThreeSame

myPlusCommutes : (n : Nat) -> (m : Nat) -> n + m = m + n
myPlusCommutes Z m = sym $ plusZeroRightNeutral m
myPlusCommutes (S k) m = rewrite myPlusCommutes k m in plusSuccRightSucc m k 

data Vect : (len : Nat) -> (elem : Type) -> Type where
  Nil  : Vect Z elem
  (::) : (x : elem) -> (xs : Vect len elem) -> Vect (S len) elem

myReverse : Vect n a -> Vect n a
myReverse xs = reverse' [] xs
  where 
    reverse' : Vect n a -> Vect m a -> Vect (n+m) a
    reverse' {n} acc [] = 
      rewrite plusZeroRightNeutral n in acc 
    reverse' {n} {m = S len} acc (x :: xs) = 
      rewrite sym (plusSuccRightSucc n len) in (reverse' (x::acc) xs)

headUnequal : DecEq a => {xs : Vect n a} -> {ys : Vect n a} -> (contra : (x = y) -> Void) -> ((x :: xs) = (y :: ys)) -> Void
headUnequal contra Refl = contra Refl

tailUnequal : DecEq a => {xs : Vect n a} -> {ys : Vect n a} -> (contra : (xs = ys) -> Void) -> ((x :: xs) = (y :: ys)) -> Void
tailUnequal contra Refl = contra Refl

DecEq a => DecEq (Vect n a) where
  decEq [] [] = Yes Refl
  decEq (x :: xs) (y :: ys) = 
    case decEq x y of 
      Yes Refl => 
        case decEq xs ys of
          Yes Refl => Yes Refl
          No contraTail => No (tailUnequal contraTail)
      No contraHead => No (headUnequal contraHead)
 