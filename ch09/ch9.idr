module Main

data Elem : a -> List a -> Type where
  Here : Elem x (x :: xs)
  There : (later : Elem x xs) -> Elem x (_ :: xs)

data Last : List a -> a -> Type where
  LastOne : Last [value] value
  LastCons : (prf : Last xs value) -> Last (_ :: xs) value  

noEmptyLast : Not (Last [] value)
noEmptyLast LastOne impossible

noSingleton : (contra : Not (x = value)) -> Not (Last [x] value)
noSingleton contra LastOne = contra Refl

noTail : (contra : Not (Last (x :: xs) value)) -> Not (Last (_ :: x :: xs) value)
noTail contra (LastCons prf) = contra prf

isLast : DecEq a => (xs : List a) -> (value : a) -> Dec (Last xs value)  
isLast [] _ = No noEmptyLast
isLast [x] value = 
  case decEq x value of 
    Yes Refl => Yes LastOne
    No contra => No $ noSingleton contra
isLast (_ :: x :: xs) value = 
  case isLast (x :: xs) value of  
    Yes prf => Yes $ LastCons prf
    No contra => No $ noTail contra